package com.pearson.lr.user.test;

import static org.junit.Assert.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.pearson.lr.user.test.dto.UserCreationDto;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class UserCreation extends BaseClass {
	
	private static Logger APP_LOG = LogManager.getLogger(UserCreation.class);
	
	String userId = null;
	String username = null;
	
	@Before
	public void setUp() {
		RestAssured.baseURI = getEndpont();
		RestAssured.port = 443;
		System.out.println("********************");
		APP_LOG.info("Succssfully Read the URL " + RestAssured.baseURI );
	}
	
	//New User Creation 
	@Test
	public void userCreation() {
		Response userResponse = RestAssured.given()
				.auth().basic("admin","password")
				.log().ifValidationFails()
				.contentType("application/json")
				.body(new UserCreationDto("TestUser","sss","ADMIN"))
				.post("/users");
		
		System.out.println("********* User Creation  ***********");
		System.out.println(RestAssured.baseURI+"/users");
		System.out.println("********************");
		System.out.println(userResponse.getBody().asString());
		System.out.println(userResponse.getStatusCode());
		
		if(userResponse.getStatusCode() !=201) {
			APP_LOG.info("Eror in user Creation");
			APP_LOG.error(userResponse.getStatusCode());
		}
		else
			APP_LOG.info("Succssfully Created the user " + userResponse.getStatusCode() );
		
		assertEquals(201,userResponse.getStatusCode());
		
		userId = userResponse.jsonPath().getString("userId");
		username =  userResponse.jsonPath().getString("username");
		
		
		assertEquals(userResponse.jsonPath().getString("username"), "TestUser");
		assertTrue(userId.length()!=0);
		assertEquals(userResponse.jsonPath().getString("userRole"), "ADMIN");
	}
	

}
