package com.pearson.lr.user.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class BaseClass {
	
	private static Logger APP_LOG = LogManager.getLogger(BaseClass.class);
	
public String getEndpont() {
		
		String env = null;
		String endPoint= null;
		
		Properties mavenProps = new Properties();
		InputStream input = null;
		input = getClass().getClassLoader().getResourceAsStream("maven.properties");
		
		try {
			mavenProps.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			APP_LOG.info("Cannot read from property file");
		}
		endPoint = mavenProps.getProperty("endPoint");
		return endPoint;
	}

}
