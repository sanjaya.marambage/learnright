package com.pearson.lr.user.test.dto;

public class UserCreationDto {
	
//	private String userFname;
	private String password;
	private String userRole;
	private String username;
	
	public String getUserName() {
		return username;
	}



	public void setUserName(String userName) {
		this.username = userName;
	}
	


	public String getPassword(){
		return this.password;
	}


	public void setPassword(String password) {
		this.password = password;
	}



	public String getUserRole() {
		return userRole;
	}



	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}



	public UserCreationDto(String userName,String password, String userRole) {
		this.username = userName;

		this.password = password;
		this.userRole = userRole;
	}

}
