package com.pearson.lr.user.test.dto;

public class PiAuthTokenDto {
	
	private String userName;
	private String password;
	
	
	public PiAuthTokenDto(String PI_USER_NAME, String PI_PASSWORD) {
		super();
		this.userName = PI_USER_NAME;
		this.password = PI_PASSWORD;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
